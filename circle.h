#ifndef CIRCLE_H
#define CIRCLE_H

#include "point.h"

namespace geometry {
    //Represents a circle in 2D space
    class Circle {
        public:
            Circle(geometry::Point& center, int radius);
            geometry::Point* get_center(); // gets center point of the circle
            int get_radius(); // gets radius of circle
        private:
            geometry::Point center; // center of the circle
            int radius; // radius of circle

    };
}

#endif CIRCLE_H