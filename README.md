# Line Intersection Challenge

Function finds the number of intersections between the equations
```math
{(x-p)}^2 + {(y-q)}^2 = r^2

y = mx + b
```
Solving this system of equations produces an quadratic equation
```math
(m^2+1)x^2 + 2(bm-qm-p)x + p^2 + b^2 -2bq + q^2 - r^2 = 0
```

Which reduces the problem down to finding the discriminant

```math
4(bm - qm - p)^2 - 4(m^2 + 1)(p^2 + b^2 -2bq + q^2 - r^2)
```

This translates to 3 cases:
- `Discriminant > 0` -> 2 intersections
- `Discriminant = 0` -> 1 intersection
- `Discriminant < 0` -> 0 intersection
