#ifndef LINE_H
#define LINE_H

#include "point.h"

namespace geometry {
    // Represents a line in 2D space defined by 2 points
    class Line {
        public:
            Line(geometry::Point& p1, geometry::Point& p2);
            geometry::Point* get_p1();
            geometry::Point* get_p2();
            double get_slope(); // gets the slope of the line
            double get_y_inter(); // gets the y intercept of the line, assuming line is on x-y plane

        private:
            geometry::Point p1; // point one on the line
            geometry::Point p2; // point two on the line
    };
}

#endif LINE_H