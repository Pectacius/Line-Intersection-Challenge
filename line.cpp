#include "line.h"

geometry::Line::Line(geometry::Point& point1, geometry::Point& point2) {
    p1 = point1;
    p2 = point2;
}

geometry::Point* geometry::Line::get_p1() {
    return &p1;
}

geometry::Point* geometry::Line::get_p2() {
    return &p2;
}

double geometry::Line::get_slope() {
    int dy = p1.get_y_coord() - p2.get_y_coord();
    int dx = p1.get_x_coord() - p2.get_x_coord();
    return dy/dx;

}
double geometry::Line::get_y_inter() {
    return p1.get_y_coord() - get_slope()*p1.get_x_coord();
}