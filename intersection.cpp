#include <cmath>
#include "line.h"
#include "circle.h"


// Finds number of intersections between a circle and a line using the discriminant
int numberOfIntersections(geometry::Circle &circle, geometry::Line &line) {
    double m = line.get_slope();
    double b = line.get_y_inter();
    
    int p = circle.get_center()->get_x_coord();
    int q = circle.get_center()->get_y_coord();
    int r = circle.get_radius();

    double discriminant = 4*std::pow(b*m - q*m - p, 2) - 4*(m*m + 1)*(p*p + b*b -2*b*q + q*q - r*r);
    if (discriminant > 0) {
        return 2;
    }
    else if (discriminant < 0) {
        return 0;
    }
    else {
        return 1;
    }
}