#include "point.h"

geometry::Point::Point() {
    x_coord = 0;
    y_coord = 0;
}

geometry::Point::Point(int x, int y) {
    x_coord = x;
    y_coord = y;
}

int geometry::Point::get_x_coord() {
    return x_coord;
}

int geometry::Point::get_y_coord() {
    return y_coord;
}