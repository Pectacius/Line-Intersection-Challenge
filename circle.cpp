#include "circle.h"

geometry::Circle::Circle(geometry::Point& c, int r) {
    center = c;
    radius = r;
}

geometry::Point* geometry::Circle::get_center() {
    return &center;
}

int geometry::Circle::get_radius() {
    return radius;
}
