#ifndef POINT_H
#define POINT_H

namespace geometry {
    // Represents a 2D point in space
    class Point {
        public:
            Point();
            Point(int x, int y);
            int get_x_coord(); // gets X coordinate of point
            int get_y_coord(); // gets Y coordinate of point
        private:
            int x_coord; // X coordinate of point
            int y_coord; // Y coordinate of point
    };
}

#endif